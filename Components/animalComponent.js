/**
 * Container for animal picture
 */

import React, {Component} from 'react';
import {StyleSheet, View, Image, Alert, TouchableHighlight} from 'react-native';

var Sound = require('react-native-sound');



export default class Animal extends Component {
    state = { clicked: false };

    _playSound(name, index) {
        this.setState(previousState => (
          { clicked: !previousState.clicked }
        ));
        var sound = this.state.clicked ? name : index;
        sound = sound + '.wav';
        // Alert.alert(sound);
        var whoosh = new Sound(sound, Sound.MAIN_BUNDLE, (error) => {
          if (error) {
            console.log('failed to load the sound', error);
            return;
          }
          else {
            // Play the sound with an onEnd callback
            whoosh.play((success) => {
            if (success) {
              console.log('successfully finished playing');
            } else {
              console.log('playback failed due to audio decoding errors');
              // reset the player to its uninitialized state (android only)
              // this is the only option to recover after an error occured and use the player again
              whoosh.reset();
            }
            });
          }
          // loaded successfully
          console.log('duration in seconds: ' + whoosh.getDuration() + 'number of channels: ' + whoosh.getNumberOfChannels());
        });

        
    }

  render() { 
    images = {
      cow : require('./Assets/assets/pictures/cow_med.jpg'),
      sheep : require('./Assets/assets/pictures/sheep_med.jpg'),
      pig : require('./Assets/assets/pictures/pig_med.jpg'),
      horse : require('./Assets/assets/pictures/horse_med.jpg')
    }
    return (
        <TouchableHighlight style={styles.container} onPress={this._playSound.bind(this, this.props.name, this.props.index)}>
            <Image resizeMode="repeat" source={images[this.props.name]} />
        </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  image: {
    flex: 1,
    resizeMode: 'contain'
  },
  container: {
    flex: 1,
  }
});