/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button, Alert} from 'react-native';
import Animal from './Components/animalComponent';

export default class App extends Animal {

  render() {
    return (
      <View style={styles.container}>
         <View style={styles.row}>
          <Animal name='cow' index='cow_name' style={styles.item}></Animal>
          <Animal name='sheep' index='sheep_name' style={styles.item}></Animal>
        </View>
        <View style={styles.row}>
          <Animal name='pig' index='pig_name' style={styles.item}></Animal>
          <Animal name='horse' index='horse_name' style={styles.item}></Animal>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  row: {
    flex: 1,
    flexDirection: 'row',
  },
  item: {
    flex: 1,
    overflow: 'hidden',
    alignItems: 'center',
    backgroundColor: 'orange',
    position: 'absolute',
    margin: 10,
  },
});
